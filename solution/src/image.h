#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct pixel {
  uint8_t b, g, r;
};

/* WARNING:
 * IMAGE_MANAGE assumes, requires and establishes
 * that the holder of the designated variable owns the object's data;
 * and ties the data lifetime to that variable whenever
 * a valid not-noop deallocator was used.
 *
 * allocator a function providing a buffer of a required size
 * deallocator is used for IMAGE_MANAGE, set to NULL for a noop */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
struct image {
  bool owner;
  void *(*allocator)(size_t bytes);
  void (*deallocator)(void *);

  uint64_t width, height;
  struct pixel *data;
};
#pragma clang diagnostic pop

#define IMAGE_MANAGE __attribute__((cleanup(image_cleanup)))
void image_cleanup(const struct image *image);

// NOTE: deallocator = NULL, for a noop dealloc
struct image image_create(uint64_t width, uint64_t height,
                          void *(*allocator)(size_t bytes),
                          void (*deallocator)(void *));
struct image image_inject(uint64_t width, uint64_t height, struct pixel *data,
                          void (*deallocator)(void *));
struct image image_move(struct image *image_to_own);
