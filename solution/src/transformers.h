#pragma once

#include "image.h"

struct image rotate270(struct image *original, void *(*allocator)(size_t),
                       void (*deallocator)(void *));
