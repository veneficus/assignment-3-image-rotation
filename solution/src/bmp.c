#include "bmp.h"
#include "log.h"
#include "utils.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <unistd.h>

// https://en.wikipedia.org/wiki/BMP_file_format [1]
struct bmp_header {
  // Bitmap file header
  uint8_t bfType1;      // 'B'
  uint8_t bfType2;      // 'M'
  uint32_t bfileSize;   // The size of the BMP file in bytes
  uint16_t bfReserved1; // Reserved; actual value depends on the application
                        // that creates the image, if created manually can be 0
  uint16_t bfReserved2; // Reserved; actual value depends on the application
                        // that creates the image, if created manually can be 0
  uint32_t bOffBits; // The offset, i.e. starting address, of the byte where the
                     // bitmap image data (pixel array) can be found.
  // DIB header, BITMAPINFOHEADER variation
  uint32_t biSize;        // the size of the DIB part of the header,
                          // always 40 for this particular implementation
  uint32_t biWidth;       // the bitmap width in pixels (signed integer)
  uint32_t biHeight;      // the bitmap height in pixels (signed integer)
  uint16_t biPlanes;      // the number of color planes (must be 1)
  uint16_t biBitCount;    // the number of bits per pixel
  uint32_t biCompression; // the compression method being used, see link [1]
  uint32_t biSizeImage;   // the image size. This is the size of the raw bitmap
                          // data; a dummy 0 can be given for BI_RGB bitmaps.
  uint32_t biXPelsPerMeter; // the horizontal resolution of the image. (pixel
                            // per metre, signed integer)
  uint32_t biYPelsPerMeter; // the vertical resolution of the image. (pixel per
                            // metre, signed integer)
  uint32_t biClrUsed;      // the number of colors in the color palette, or 0 to
                           // default to 2n
  uint32_t biClrImportant; //  the number of important colors used, or 0 when
                           //  every color is important; generally ignored
} __attribute__((packed));

struct bmp_header bmp_header_construct(const struct image *const image) {
  const uint64_t imageSize =
      image->height * image->width * sizeof(*(image->data));
  const uint64_t fileSize = sizeof(struct bmp_header) + imageSize;

  assert(imageSize <= UINT32_MAX);
  assert(fileSize <= UINT32_MAX);
  assert(image->width <= UINT32_MAX);
  assert(image->height <= UINT32_MAX);

  struct bmp_header result = (struct bmp_header){0};
  result.bfType1 = 'B';
  result.bfType2 = 'M';
  result.bfileSize = (uint32_t)fileSize;
  result.bOffBits = sizeof(result);
  result.biSize = 40;
  result.biWidth = (uint32_t)image->width;
  result.biHeight = (uint32_t)image->height;
  result.biPlanes = 1;
  result.biBitCount = 24;
  result.biSizeImage = (uint32_t)imageSize;

  return result;
}

__attribute__((pure)) static bool
bmp_header_verify_integrity(const struct bmp_header *const header) {
  // TODO: now there are type1 and type2 fields to verify this easier
  const unsigned char *header_pointer = (const unsigned char *)header;
  static_assert('M' == 0x4D && 'B' == 0x42, "Necessary expectations not met");
  static_assert(sizeof(unsigned char) == sizeof(uint8_t), "I give up");

  if (*header_pointer != 'B' || *(header_pointer + 1) != 'M') {
    debug_message("Type signature verification v2 failed");
    return false;
  }

  assert(header->biSize == 40);
  assert(header->biPlanes == 1);
  assert(header->biBitCount == 24);

  return true;
}

static bool load_header(struct readonly_file_mapping file,
                        struct bmp_header *const header) {
  if (!file.data) {
    return false;
  }
  *header = *((const struct bmp_header *)file.data);
  return true;
}

// and would be detrimental for the 5th task
size_t bmp_necessary_file_size(const struct image *const image) {
  return sizeof(struct bmp_header) +
         (image->height * image->width * sizeof(*(image->data))) +
         ((image->width % 4) * image->height);
}

struct image bmp_get_image(struct readonly_file_mapping file,
                           void *(*allocator)(size_t bytes),
                           void (*deallocator)(void *)) {
  struct bmp_header header;
  if (!load_header(file, &header)) {
    return (struct image){0};
  }
  if (!bmp_header_verify_integrity(&header)) {
    return (struct image){0};
  }

  const uint64_t width = header.biWidth;
  const uint64_t height = header.biHeight;
  struct image result = image_create(width, height, allocator, deallocator);
  const unsigned char *const bitmap = file.data + header.bOffBits;

  for (size_t row = 0; row < height; row++) {
    for (size_t column = 0; column < width; column++) {
      result.data[row * width + column].b =
          bitmap[((row * width + column) * 3) + (row * (width % 4))];
      result.data[row * width + column].g =
          bitmap[((row * width + column) * 3) + 1 + (row * (width % 4))];
      result.data[row * width + column].r =
          bitmap[((row * width + column) * 3) + 2 + (row * (width % 4))];
    }
  }

  return result;
}

void bmp_save_image(struct readwrite_file_mapping file,
                    const struct image *const image) {
  assert(file.data);
  assert(image);
  assert(file.mapped_size >= bmp_necessary_file_size(image));

  struct bmp_header header = bmp_header_construct(image);
  assert(bmp_header_verify_integrity(&header));

  const uint64_t width = header.biWidth;
  const uint64_t height = header.biHeight;
  unsigned char *const bitmap = file.data + header.bOffBits;

  *((struct bmp_header *)file.data) = header;

  // NOTE to future self:
  // making a struct that would hold 192 bytes could
  // be advantagous due to math.lcd(64,24) equaling 192
  // and AVX512 supporting up to 64bytes of data
  // giving us convenient autovectorization
  // but that is too much of a bother
  // and would be detrimental for the 5th task
  for (size_t row = 0; row < height; row++) {
    for (size_t column = 0; column < width; column++) {
      *((struct pixel *)(bitmap + ((row * width + column) * 3) +
                         (row * (width % 4)))) =
          (image->data)[row * width + column];
    }
    for (size_t pad = 0; pad < (width % 4); pad++) {
      bitmap[((row * width + width) * 3) + 2 + (row * (width % 4))] = 0;
    }
  }
}
