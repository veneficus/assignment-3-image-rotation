#include "log.h"

#include <stdbool.h>
#include <stdio.h>

/* TODO: perhaps debug levels here through enum with
 * switch with fallthrough in debug_message() */
#ifdef DEBUG
const static bool debug_state = true;
#else
const static bool debug_state = false;
#endif

void debug_message(const char *const message) {
  if (debug_state)
    puts(message);
}
