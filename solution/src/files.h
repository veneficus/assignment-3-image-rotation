#pragma once

#include "utils.h"

#include <stdio.h>

/* Holders for mmap-ed pointers */
/* Might consider adding alikes of mmap flags to the holders */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
struct file_holder {
  const int fd;
  unsigned char *const data;
  const size_t mapped_size;
  // mode: O_RDONLY, O_WRONLY, or O_RDWR from fcntl
  // current implementation never creater a WRONLY file
  int mode;
};
struct readonly_file_mapping {
  const unsigned char *const data;
  const size_t mapped_size;
};
struct readwrite_file_mapping {
  unsigned char *const data;
  const size_t mapped_size;
};
#pragma clang diagnostic pop

/* WARNING:
 * FILE_MANAGE assumes, requires and establishes
 * that the holder of the designated variable owns the object's data;
 * and ties the data lifetime to that variable */
#define FILE_MANAGE __attribute__((cleanup(file_cleanup)))
void file_cleanup(const struct file_holder *file_holder);

struct file_holder open_ro_file(const char *filepath);
struct file_holder open_rw_file(const char *filepath);
struct file_holder open_rw_sized_file(const char *filepath, size_t size);
struct readonly_file_mapping get_ro_mapping(struct file_holder file);
struct readwrite_file_mapping get_rw_mapping(struct file_holder file);
