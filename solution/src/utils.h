#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Some minimal sanity checking */
bool system_memory_limit_verification(uint64_t bytesRequired);

/* To avoid multiply induced overflow checking strange behaviour */
__attribute__((const)) bool multipliable(uint64_t a, uint64_t b);
