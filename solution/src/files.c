#define _XOPEN_SOURCE 700

#include "files.h"
#include "log.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/mman.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

void file_cleanup(const struct file_holder *const file) {
  if (file) {
    if (file->data) {
      msync(file->data, file->mapped_size, MS_SYNC);
      munmap(file->data, file->mapped_size);
    }
    if (file->fd != -1) {
      fsync(file->fd);
      close(file->fd);
    }
    debug_message("file_handler deallocator invoked");
  }
}

static struct file_holder open_file(const char *const filepath, const int flags,
                                    size_t size, const bool size_from_file) {
  assert(filepath);
  assert(flags == O_RDONLY || flags == O_RDWR);

  int mmap_prot = PROT_READ;
  if (flags == O_RDWR) {
    mmap_prot |= PROT_WRITE;
  }

  const int fd = open(filepath, flags | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO);
  assert(fd != -1);

  if (size_from_file) {
    struct stat stat;
    assert(fstat(fd, &stat) != -1);

    assert(stat.st_size > 0);
    assert((unsigned long long)stat.st_size <= SIZE_MAX);
    size = (size_t)stat.st_size;
  }

  if (flags == O_RDWR) {
    const int code = ftruncate(fd, (off_t)size);
    assert(code == 0);
  }

  void *const pointer = mmap(NULL, size, mmap_prot, MAP_SHARED_VALIDATE, fd, 0);

  assert(pointer);

  return (struct file_holder){
      .fd = fd, .data = pointer, .mapped_size = size, .mode = flags};
}

struct file_holder open_ro_file(const char *const filepath) {
  return open_file(filepath, O_RDONLY, 0, true);
}
struct file_holder open_rw_file(const char *const filepath) {
  return open_file(filepath, O_RDWR, 0, true);
}
struct file_holder open_rw_sized_file(const char *filepath, size_t size) {
  return open_file(filepath, O_RDWR, size, false);
}

struct readonly_file_mapping get_ro_mapping(struct file_holder file) {
  return (struct readonly_file_mapping){.data = file.data,
                                        .mapped_size = file.mapped_size};
}
struct readwrite_file_mapping get_rw_mapping(struct file_holder file) {
  return (struct readwrite_file_mapping){.data = file.data,
                                         .mapped_size = file.mapped_size};
}
