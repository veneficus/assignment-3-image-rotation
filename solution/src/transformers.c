#include "transformers.h"

struct image rotate270(struct image *original, void *(*allocator)(size_t),
                       void (*deallocator)(void *)) {
  struct image result =
      image_create(original->height, original->width, allocator, deallocator);

  for (size_t x = 0; x < result.width; x++) {
    for (size_t y = 0; y < result.height; y++) {
      result.data[y * result.width + x] =
          original->data[(original->height - x) * original->width + y -
                         original->width];
    }
  }

  return result;
}
