#include "bmp.h"
#include "image.h"
#include "transformers.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HELP_STRING                                                            \
  "Usage example:\n"                                                           \
  "./binary inputImage outputImage\n"

__attribute__((noreturn)) static void fail(const char *const fail_message) {
  puts(fail_message);
  abort();
}

int main(const int argc, const char *const *const argv) {
  if (argc != 3) {
    fail("Erroneous usage.\n" HELP_STRING);
  }

  FILE_MANAGE struct file_holder const read_file = open_ro_file(argv[1]);

  IMAGE_MANAGE struct image image =
      bmp_get_image(get_ro_mapping(read_file), malloc, free);

  IMAGE_MANAGE struct image const rotated_image = rotate270(&image, malloc, free);

  FILE_MANAGE struct file_holder const write_file =
      open_rw_sized_file(argv[2], bmp_necessary_file_size(&rotated_image));

  bmp_save_image(get_rw_mapping(write_file), &rotated_image);
  return 0;
}
