#pragma once

#include "files.h"
#include "image.h"

#include <stdint.h>
#include <stdio.h>

// header is fit to the image passed into the function
struct bmp_header bmp_header_construct(const struct image *const image);
size_t bmp_necessary_file_size(const struct image *const image);
void bmp_save_image(struct readwrite_file_mapping file, const struct image * image);

struct image bmp_get_image(struct readonly_file_mapping file,
                           void *(*allocator)(size_t bytes),
                           void (*deallocator)(void *));
