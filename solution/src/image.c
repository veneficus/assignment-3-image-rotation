#include "image.h"
#include "log.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

void image_cleanup(const struct image *const image) {
  if (image) {
    if (image->owner && image->deallocator) {
      (*(image->deallocator))(image->data);
      debug_message("image deallocator invoked");
    }
  }
}

static struct image image_construct(bool owner, void *(*allocator)(size_t),
                                    void (*deallocator)(void *), uint64_t width,
                                    uint64_t height, struct pixel *data) {
  return (struct image){.owner = owner,
                        .allocator = allocator,
                        .deallocator = deallocator,
                        .width = width,
                        .height = height,
                        .data = data};
}

struct image image_create(uint64_t width, uint64_t height,
                          void *(*allocator)(size_t),
                          void (*deallocator)(void *)) {
  assert(allocator);
  struct pixel *const data =
      (struct pixel *)((*allocator)(sizeof(struct pixel) * width * height));

  return image_construct(true, allocator, deallocator, width, height, data);
}

struct image image_inject(uint64_t width, uint64_t height, struct pixel *data,
                          void (*deallocator)(void *)) {
  assert(data);
  return image_construct(true, NULL, deallocator, width, height, data);
}

struct image image_move(struct image *image_to_own) {
  assert(image_to_own->owner);
  image_to_own->owner = false;
  return image_inject(image_to_own->width, image_to_own->height,
                      image_to_own->data, image_to_own->deallocator);
}
