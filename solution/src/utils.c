#include "utils.h"

#include <unistd.h>

bool system_memory_limit_verification(const uint64_t bytesRequired) {
  const uint64_t systemPageSize = (uint64_t)sysconf(_SC_PAGESIZE);
  const uint64_t systemFreePages = (uint64_t)sysconf(_SC_AVPHYS_PAGES);
  /* Imprecision of rounding is fine here */
  const uint64_t pagesRequired = (bytesRequired / systemPageSize) + 1;
  return pagesRequired < systemFreePages;
}

__attribute__((const)) static uint64_t log2loop(uint64_t x) {
  uint64_t result = 0;
  while ((x >>= 1))
    result++;
  return result;
}

/* To avoid multiply induced overflow */
__attribute__((const)) bool multipliable(const uint64_t a, const uint64_t b) {
  return (log2loop(a) + log2loop(b) <= 64);
}
